package org.example;

import org.example.entities.Adresse;
import org.example.entities.Commande;
import org.example.entities.Product;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;


import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Demo {
    public static void main(String[] args){
        StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure().build();
        SessionFactory sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
        Session session = sessionFactory.openSession();
        session.getTransaction().begin();
        Adresse adresse = new Adresse("Rue de nédon", "Cauchy", "62260");
        List<Product> productList = new ArrayList<>();
        LocalDate date = LocalDate.now();
        Commande commande = new Commande(productList, 190,date, adresse);
        adresse.setCommande(commande);
        commande.setAdresse(adresse);
        session.save(commande);
        session.save(adresse);
        session.getTransaction().commit();
        session.close();
        sessionFactory.close();
        registry.close();

    }
}
