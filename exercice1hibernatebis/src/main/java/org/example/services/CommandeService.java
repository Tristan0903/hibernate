package org.example.services;

import org.example.entities.Commande;
import org.example.entities.Product;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import javax.management.Query;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class CommandeService {

    public static void createCommandeWProducts(){
        StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure().build();
        SessionFactory sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
        Session session = sessionFactory.openSession();
        session.getTransaction().begin();
        Scanner sc = new Scanner(System.in);
        List<Product> products = new ArrayList<>();
        double total = 0;
        Long id = 1L;
        List<Long> ids = new ArrayList<>();
        do {
            System.out.println("Veuillez entrer les id des produits de la commande : ");
             id = sc.nextLong();
             if(id == 0){
                 break;
             } else {
                 ids.add(id);
             }
        }while(id != 0L);
        for (Long idp:ids
             ) {
            Product product = (Product) session.createQuery("from Product where id=:id").setParameter("id", idp).getSingleResult();
            products.add(product);
          double prix = product.getPrix();
          total = total+prix;
        }
        Commande commande = new Commande(products, total, LocalDate.now());
        session.save(commande);
        session.getTransaction().commit();
        session.close();
        sessionFactory.close();
        registry.close();
    }

    public static void ShowCommandes(){
        StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure().build();
        SessionFactory sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
        Session session = sessionFactory.openSession();
        session.getTransaction().begin();
        List<Commande> commande = session.createQuery("from Commande").list();
        System.out.println(commande);
    }

    public static void ShowTodaysCommandes(){
        StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure().build();
        SessionFactory sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
        Session session = sessionFactory.openSession();
        session.getTransaction().begin();
        LocalDate date_commande = LocalDate.now();
        List<Commande> commandes = session.createQuery("from Commande where date_commande=:date_commande").setParameter("date_commande", date_commande).list();
        System.out.println(commandes);
    }




}
