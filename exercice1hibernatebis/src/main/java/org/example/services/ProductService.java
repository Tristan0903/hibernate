package org.example.services;

import org.example.entities.Commentaire;
import org.example.entities.Image;
import org.example.entities.Product;
import org.example.interfaces.TestInterface;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.query.Query;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class ProductService implements TestInterface {


    public static void CreateProduct(String marque, String reference, String dateAchat, double prix, int stock) throws ParseException {
        StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure().build();
        SessionFactory sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
        Session session = sessionFactory.openSession();
        session.getTransaction().begin();
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        Date date = format.parse(dateAchat);
        Product product = new Product(marque, reference, date, prix, stock);
        session.save(product);
        System.out.println("Le produit " + product.getId() + " a été créé.");
        session.close();
        registry.close();
    }


    public static void ShowProductById(Long id) {
        StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure().build();
        SessionFactory sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
        Session session = sessionFactory.openSession();
        session.getTransaction().begin();
        Product product = session.load(Product.class, id);
        System.out.println(product);
        session.close();
        registry.close();
    }

    public static void DeleteProduct(Long id) {
        StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure().build();
        SessionFactory sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
        Session session = sessionFactory.openSession();
        session.getTransaction().begin();
        Product product = session.load(Product.class, id);
        session.delete(product);
        session.getTransaction().commit();
        session.close();
        registry.close();
    }


    public static void UpdateProduct(Long id) throws ParseException {
        StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure().build();
        SessionFactory sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
        Session session = sessionFactory.openSession();
        session.getTransaction().begin();
        Product product = session.load(Product.class, id);
        Scanner sc = new Scanner(System.in);
        System.out.println("Nouvelle marque : ");
        String marque = sc.next();
        System.out.println("Nouvelle ref : ");
        String reference = sc.next();
        System.out.println("Nouvelle date (dd-MM-yyyy) : ");
        String dateString = sc.next();
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        Date date = format.parse(dateString);
        System.out.println("Nouveaux prix : ");
        double prix = sc.nextDouble();
        System.out.println("Nouveau stock : ");
        int stock = sc.nextInt();
        product.setMarque(marque);
        product.setReference(reference);
        product.setDateAchat(date);
        product.setPrix(prix);
        product.setStock(stock);
        session.update(product);
        session.getTransaction().commit();
        session.close();
        registry.close();
    }

    public static void ShowAllProducts(){
        StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure().build();
        SessionFactory sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
        Session session = sessionFactory.openSession();
        session.getTransaction().begin();
        Query<Product> productQuery = session.createQuery("from Product");
        List<Product> products = productQuery.list();
        products.forEach(System.out::println);
        session.close();
        registry.close();
        sessionFactory.close();
    }

    public static void ShowProductsOverPrice(double prix){
        StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure().build();
        SessionFactory sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
        Session session = sessionFactory.openSession();
        session.getTransaction().begin();
        Query<Product> productQuery = session.createQuery("from Product where prix > " +prix);
        List<Product> products = productQuery.list();
        products.forEach(System.out::println);
        session.close();
        registry.close();
        sessionFactory.close();
    }

    public static void ShowProductsBtwnDates(String date1, String date2){
        StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure().build();
        SessionFactory sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
        Session session = sessionFactory.openSession();
        session.getTransaction().begin();
        Query<Product> productQuery = session.createQuery("from Product where dateAchat >= "+date1+" and dateAchat <= "+date2);
        List<Product> products = productQuery.list();
        products.forEach(System.out::println);
        session.close();
        registry.close();
        sessionFactory.close();
    }

    public static void ShowProductsByRefAStock(int stock){
        StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure().build();
        SessionFactory sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
        Session session = sessionFactory.openSession();
        session.getTransaction().begin();
        Query<Product> productQuery = session.createQuery("select id, reference from Product where stock <" +stock);
        List<Product> products = productQuery.list();
        List<Product> products1 = new ArrayList<>();
        for (Object p:
             products) {
            Object[] prod =(Object[]) p;
            Product product =new Product();
            product.setId((Long) prod[0]);
            product.setReference((String) prod[1]);
            products1.add(product);
        } products1.forEach(System.out::println);
    }

    public static void ShowLotoProducts(){
        StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure().build();
        SessionFactory sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
        Session session = sessionFactory.openSession();
        session.getTransaction().begin();
        Query query = session.createQuery("select sum(prix) from Product where marque =:marque").setParameter("marque","Lotto");
        Double prix = (Double) query.uniqueResult();
        System.out.println("La valeur du stock de la marque lotto " +prix);
    }


    public static void CalculAvg(){
        StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure().build();
        SessionFactory sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
        Session session = sessionFactory.openSession();
        session.getTransaction().begin();
        Query<Double> query = (Query<Double>) session.createQuery("select avg(prix) from Product");
        double avg_prix = query.uniqueResult();
        System.out.println(avg_prix);
    }

    public static void ShowProdMarques(){
        StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure().build();
        SessionFactory sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
        Session session = sessionFactory.openSession();
        session.getTransaction().begin();
        List<String> marquesList = new ArrayList<>();
        marquesList.add("Adidas");
        marquesList.add("Nike");
        marquesList.add("Lotto");
        Query<Product> productQuery = session.createQuery("from Product where marque in :marquesList").setParameter("marquesList",marquesList);
        productQuery.list().forEach(System.out::println);
    }

    public static void DeleteReebokProducts(){
        StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure().build();
        SessionFactory sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
        Session session = sessionFactory.openSession();
        session.getTransaction().begin();
        Query query = session.createQuery("delete from Product where marque =:marque").setParameter("marque", "Reebok");
        int success = query.executeUpdate();
        System.out.println(success);
    }

    public static void AddComment(){
        StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure().build();
        SessionFactory sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
        Session session = sessionFactory.openSession();
        session.getTransaction().begin();
        Scanner sc = new Scanner(System.in);
        System.out.println("Veuillez saisir l'id du produit auquel ajouter le commentaire: ");
        Long id = sc.nextLong();
        Product product1 = (Product) session.createQuery("from Product where id =:id").setParameter("id",id).getSingleResult();
        System.out.println("Veuillez écrire votre commentaire : ");
        String contenu = sc.nextLine();
        contenu = sc.nextLine();
        System.out.println("Veuillez saisir la note du produit : ");
        double note = sc.nextInt();
        Date date = new Date();
        Commentaire commentaire = new Commentaire(contenu, date, note, product1);
        session.save(commentaire);
        session.getTransaction().commit();
    }

    public static void AddImg(){
        StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure().build();
        SessionFactory sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
        Session session = sessionFactory.openSession();
        session.getTransaction().begin();
        Scanner sc = new Scanner(System.in);
        System.out.println("Veuillez saisir l'id du produit auquel ajouter l'image': ");
        Long id = sc.nextLong();
        Product product1 = (Product) session.createQuery("from Product where id =:id").setParameter("id",id).getSingleResult();
        System.out.println("Veuillez écrire l'url de l'image : ");
        String url = sc.nextLine();
        url = sc.nextLine();
        Image image = new Image(url, product1);
        session.save(image);
        session.getTransaction().commit();
    }

    public static void ShowProductsNoteAbv4() {
        StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure().build();
        SessionFactory sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
        Session session = sessionFactory.openSession();
        session.getTransaction().begin();
        List<Product> products = session.createQuery("select distinct product from Commentaire where note>=4").getResultList();
        System.out.println(products);
    }
}
