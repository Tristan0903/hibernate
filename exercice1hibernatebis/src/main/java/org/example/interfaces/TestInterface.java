package org.example.interfaces;

import java.text.ParseException;

public interface TestInterface {

    public static void CreateProduct(String marque, String reference, String dateAchat, double prix, int stock) {

    }

    public static void ShowProductById(Long id) {

    }

    public static void DeleteProduct(Long id) {

    }

    public static void UpdateProduct(Long id) throws ParseException {

    }

    public static void ShowAllProducts(){

    }
}
