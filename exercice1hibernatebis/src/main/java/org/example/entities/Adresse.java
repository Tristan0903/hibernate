package org.example.entities;

import javax.persistence.*;

@Entity
@Table(name = "adresse")
public class Adresse {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long adresse_id;

    private String rue;

    private String ville;

    @Column(length = 5)
    private String code_postal;
    @OneToOne(mappedBy = "adresse")
    private Commande commande;

    public Adresse() {
    }

    public Adresse(String rue, String ville, String code_postal, Commande commande) {
        this.rue = rue;
        this.ville = ville;
        this.code_postal = code_postal;
        this.commande = commande;
    }

    public Adresse(String rue, String ville, String code_postal) {
        this.rue = rue;
        this.ville = ville;
        this.code_postal = code_postal;
    }

    public Long getAdresse_id() {
        return adresse_id;
    }

    public void setAdresse_id(Long adresse_id) {
        this.adresse_id = adresse_id;
    }

    public String getRue() {
        return rue;
    }

    public void setRue(String rue) {
        this.rue = rue;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getCode_postal() {
        return code_postal;
    }

    public void setCode_postal(String code_postal) {
        this.code_postal = code_postal;
    }

    public Commande getCommande() {
        return commande;
    }

    public void setCommande(Commande commande) {
        this.commande = commande;
    }

    @Override
    public String toString() {
        return "Adresse{" +
                "adresse_id=" + adresse_id +
                ", rue='" + rue + '\'' +
                ", ville='" + ville + '\'' +
                ", code_postal='" + code_postal + '\'' +
                ", commande=" + commande +
                '}';
    }
}
