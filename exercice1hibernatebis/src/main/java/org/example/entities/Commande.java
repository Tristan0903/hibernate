package org.example.entities;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "commande")
public class Commande {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id_commande;
    @OneToMany(mappedBy = "commande")
    List<Product> productList = new ArrayList<>();
    private double total;
    private LocalDate date_commande;

    @OneToOne
    @JoinColumn(name = "adresse_id")
    private Adresse adresse;

    public Commande() {
    }

    public Commande(List<Product> productList, double total, LocalDate date_commande) {
        this.productList = productList;
        this.total = total;
        this.date_commande = date_commande;
    }

    public Commande(List<Product> productList, double total, LocalDate date_commande, Adresse adresse) {
        this.productList = productList;
        this.total = total;
        this.date_commande = date_commande;
        this.adresse = adresse;
    }

    public Long getId_commande() {
        return id_commande;
    }

    public void setId_commande(Long id_commande) {
        this.id_commande = id_commande;
    }

    public List<Product> getProductList() {
        return productList;
    }

    public void addProduct(Product p){
        this.productList.add(p);
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public LocalDate getDate_commande() {
        return date_commande;
    }

    public void setDate_commande(LocalDate date_commande) {
        this.date_commande = date_commande;
    }

    public Adresse getAdresse() {
        return adresse;
    }

    public void setAdresse(Adresse adresse) {
        this.adresse = adresse;
    }

    @Override
    public String toString() {
        return "Commande{" +
                "id_commande=" + id_commande +
                ", productList=" + productList +
                ", total=" + total +
                ", date_commande=" + date_commande +
                ", adresse=" +adresse+
                '}';
    }
}
