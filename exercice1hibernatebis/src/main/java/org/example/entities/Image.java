package org.example.entities;

import javax.persistence.*;

@Entity
@Table(name = "images")
public class Image {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id_img;

    private String url;

    @ManyToOne
    @JoinColumn(name = "image_id")
    private Product product;

    public Image() {
    }

    public Image(String url, Product product) {
        this.url = url;
        this.product = product;
    }



    public Long getId_img() {
        return id_img;
    }

    public void setId_img(Long id_img) {
        this.id_img = id_img;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "Image{" +
                "id_img=" + id_img +
                ", url='" + url + '\'' +
                '}';
    }
}
