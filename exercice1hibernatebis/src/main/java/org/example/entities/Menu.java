package org.example.entities;

import org.example.services.CommandeService;
import org.example.services.ProductService;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Scanner;

public class Menu {
    public static void ShowMenu(){
        System.out.println("--- Menu Produits ---");
        System.out.println("1. Créer un produit");
        System.out.println("2. Trouver un produit par id");
        System.out.println("3. Supprimer un produit par id");
        System.out.println("4. Modifier un produit par id");
        System.out.println("5. Afficher la liste des produits");
        System.out.println("6. Afficher les produits dont le prix est > à 100");
        System.out.println("7. Afficher les produits compris entre deux dates");
        System.out.println("8. Afficher les produits dont le stock est inférieur à la saisie");
        System.out.println("9. Afficher la valeur du stock des produits de la marque Lotto");
        System.out.println("10. Calculer le prix moyen des produits");
        System.out.println("11. Récuprérer la liste des produits de marque de vêtements");
        System.out.println("12. Supprimer les produits de la marque Reebook de la table Product");
        System.out.println("13. Ajouter un commentaire à un produit");
        System.out.println("14. Ajouter une image à un produit");
        System.out.println("15. Afficher les produits dont la note est supérieure a 4");
        System.out.println("16. Créer une commande avec un/des produits");
        System.out.println("17. Afficher la totalité des commandes");
        System.out.println("18. Afficher les commandes du jour");
        System.out.println("19. Exit");
    }

    public static int AskChoice(){
        Scanner sc = new Scanner(System.in);
        int choix = sc.nextInt();
        return choix;
    }

    public static void case1() throws ParseException {
        Scanner sc = new Scanner(System.in);
        System.out.println("Marque produit : ");
        String marque = sc.next();
        System.out.println("Ref produit : ");
        String reference = sc.next();
        System.out.println("Date produit (dd-MM-yyyy) : ");
        String dateString = sc.next();
        System.out.println("Prix produit : ");
        double prix = sc.nextDouble();
        System.out.println("Stock produit : ");
        int stock = sc.nextInt();
        ProductService.CreateProduct(marque, reference, dateString, prix, stock);
    }

    public static void case2() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Veuillez entrer l'id: ");
        Long id_show = sc.nextLong();
        ProductService.ShowProductById(id_show);
    }

    public static void case3(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Veuillez entrer l'id: ");
        Long id_del = sc.nextLong();
        ProductService.DeleteProduct(id_del);
    }

    public static void case4() throws ParseException {
        Scanner sc = new Scanner(System.in);
        System.out.println("Veuillez entrer l'id: ");
        Long id_upd = sc.nextLong();
        ProductService.UpdateProduct(id_upd);
    }

    public static void case5(){
        System.out.println("Voici la liste des produits");
        ProductService.ShowAllProducts();
    }

    public static void case6(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Veuillez entrer un prix pour voir les produits plus chers");
        double prix1 = sc.nextDouble();
        System.out.println("Voici la liste des produits dont le prix > " +prix1);
        ProductService.ShowProductsOverPrice(prix1);
    }

    public static void case7(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Veuillez entre la 1ere date ('yyyy-MM-dd') : ");
        String datestr1 = sc.next();
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
//                    Date date1 = format.parse(datestr1);
        System.out.println("Veuillez entrer la 2eme date ('yyyy-MM-dd') : ");
        String datestr2 = sc.next();
//                    Date date2 = format.parse(datestr2);
        System.out.println("Voici la liste des produits comprise entre " +datestr1+ " et " +datestr2);
        ProductService.ShowProductsBtwnDates(datestr1,datestr2);
    }

    public static void case8(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Veuillez entrer la plus grande valeur de stock: ");
        int stock1 = sc.nextInt();
        ProductService.ShowProductsByRefAStock(stock1);
    }
    public static void case9(){
        ProductService.ShowLotoProducts();
    }
    public static void case10(){
        ProductService.CalculAvg();
    }
    public static void case11(){
        ProductService.ShowProdMarques();
    }
    public static void case12(){
        ProductService.DeleteReebokProducts();
    }
    public static void case13(){
        ProductService.AddComment();
    }
    public static void case14(){
        ProductService.AddImg();
    }
    public static void case15(){
        ProductService.ShowProductsNoteAbv4();
    }
    public static void case16(){
        CommandeService.createCommandeWProducts();
    }
    public static void case17(){
        CommandeService.ShowCommandes();
    }
    public static void case18(){
        CommandeService.ShowTodaysCommandes();
    }
    public static void switchCase(int choix) throws ParseException {
        Scanner sc = new Scanner(System.in);
        switch (choix){
            case 1 :
                case1();
                break;
            case 2:
                case2();
                break;
            case 3:
                case3();
                break;
            case 4:
                case4();
                break;
            case 5:
                case5();
                break;
            case 6:
                case6();
                break;
            case 7:
                case7();
                break;
            case 8:
                case8();
                break;
            case 9:
                case9();
                break;
            case 10:
                case10();
                break;
            case 11:
                case11();
                break;
            case 12:
                case12();
                break;
            case 13:
                case13();
                break;
            case 14:
                case14();
                break;
            case 15:
                case15();
                break;
            case 16:
                case16();
                break;
            case 17:
                case17();
                break;
            case 18:
                case18();
                break;
            default:break;
        }
    }
    public static void MenuFinal() throws ParseException {
        StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure().build();
        SessionFactory sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
        Session session = sessionFactory.openSession();
        int choix = 0;
        do {
            Menu.ShowMenu();
            choix = Menu.AskChoice();
            Menu.switchCase(choix);
        } while (choix!=19);

        session.close();
        sessionFactory.close();
        registry.close();
    }
}
