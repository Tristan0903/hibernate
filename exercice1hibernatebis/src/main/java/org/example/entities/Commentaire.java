package org.example.entities;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "commentaires")
public class Commentaire {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id_com;

    private String contenu;

    private Date date;

    private double note;

    @ManyToOne
    @JoinColumn(name = "commentaire_id")
    private Product product;

    public Commentaire() {
    }

    public Commentaire(String contenu, Date date, double note, Product product) {
        this.contenu = contenu;
        this.date = date;
        this.note = note;
        this.product = product;
    }

    public Long getId_com() {
        return id_com;
    }

    public void setId_com(Long id_com) {
        this.id_com = id_com;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getNote() {
        return note;
    }

    public void setNote(double note) {
        this.note = note;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    @Override
    public String toString() {
        return "Commentaire{" +
                "id_com=" + id_com +
                ", contenu='" + contenu + '\'' +
                ", date=" + date +
                ", note=" + note +
                '}';
    }
}
