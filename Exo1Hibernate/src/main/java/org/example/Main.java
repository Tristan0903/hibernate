package org.example;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import org.example.services.ProductService;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import java.text.ParseException;

public class Main {
    public static void main(String[] args) throws ParseException {
        int choix = 0;
        StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure().build();
        SessionFactory sessionFactory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
        Session session = sessionFactory.openSession();
        session.getTransaction().begin();
        do {
            System.out.println("--- Menu Produits ---");
            System.out.println("1. Créer un produit");
            System.out.println("2. Trouver un produit par id");
            System.out.println("3. Supprimer un produit par id");
            System.out.println("4. Modifier un produit par id");
            System.out.println("5. Afficher la liste des produits");
            System.out.println("6. Afficher les produits dont le prix est > à 100");
            System.out.println("7. Afficher les produits compris entre deux dates");
            System.out.println("8. Afficher les produits dont le stock est inférieur à la saisie");
            System.out.println("9. Afficher la valeur du stock des produits de la marque Lotto");
            System.out.println("10. Calculer le prix moyen des produits");
            System.out.println("11. Récuprérer la liste des produits de marque de vêtements");
            System.out.println("12. Supprimer les produits de la marque Reebook de la table Product");
            System.out.println("13. Exit");
            Scanner sc = new Scanner(System.in);
            choix = sc.nextInt();
            switch (choix){
                case 1 :
                    System.out.println("Marque produit : ");
                    String marque = sc.next();
                    System.out.println("Ref produit : ");
                    String reference = sc.next();
                    System.out.println("Date produit (dd-MM-yyyy) : ");
                    String dateString = sc.next();
                    System.out.println("Prix produit : ");
                    double prix = sc.nextDouble();
                    System.out.println("Stock produit : ");
                    int stock = sc.nextInt();
                    ProductService.CreateProduct(marque, reference, dateString, prix, stock);
                    break;
                case 2:
                    System.out.println("Veuillez entrer l'id: ");
                    Long id_show = sc.nextLong();
                    ProductService.ShowProductById(id_show);
                    break;
                case 3:
                    System.out.println("Veuillez entrer l'id: ");
                    Long id_del = sc.nextLong();
                    ProductService.DeleteProduct(id_del);
                    break;
                case 4:
                    System.out.println("Veuillez entrer l'id: ");
                    Long id_upd = sc.nextLong();
                    ProductService.UpdateProduct(id_upd);
                    break;
                case 5:
                    System.out.println("Voici la liste des produits");
                    ProductService.ShowAllProducts();
                    break;
                case 6:
                    System.out.println("Veuillez entrer un prix pour voir les produits plus chers");
                    double prix1 = sc.nextDouble();
                    System.out.println("Voici la liste des produits dont le prix > " +prix1);
                    ProductService.ShowProductsOverPrice(prix1);
                    break;
                case 7:
                    System.out.println("Veuillez entre la 1ere date ('yyyy-MM-dd') : ");
                    String datestr1 = sc.next();
                    SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
//                    Date date1 = format.parse(datestr1);
                    System.out.println("Veuillez entrer la 2eme date ('yyyy-MM-dd') : ");
                    String datestr2 = sc.next();
//                    Date date2 = format.parse(datestr2);
                    System.out.println("Voici la liste des produits comprise entre " +datestr1+ " et " +datestr2);
                    ProductService.ShowProductsBtwnDates(datestr1,datestr2);
                    break;
                case 8:
                    System.out.println("Veuillez entrer la plus grande valeur de stock: ");
                    int stock1 = sc.nextInt();
                    ProductService.ShowProductsByRefAStock(stock1);
                    break;
                case 9:
                    ProductService.ShowLotoProducts();
                    break;
                case 10:
                    ProductService.CalculAvg();
                    break;
                case 11:
                    ProductService.ShowProdMarques();
                    break;
                case 12:
                    ProductService.DeleteReebokProducts();
                    break;
                default:break;
            }
        }while (choix != 13);
        session.close();
        sessionFactory.close();
    }
}